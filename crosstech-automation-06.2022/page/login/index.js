const { I } = inject()
const loginLocator = require('./locator')
const homePageLocator = require('../homePage/locator')
const settingPageLocator = require('../settingPage/locator');
const timeout = require('../common/timeout');
const customMethod = require('../../utils/customMethod');
module.exports = {
    login(url, email, password) {
        I.amOnPage(url);
        //điền email, mật khẩu
        I.fillField(loginLocator.emailField,email)
        I.fillField(loginLocator.passwordField,password)
        //đăng nhập
        I.click(loginLocator.loginButton)

        I.waitForElement(homePageLocator.menu.dangXuatText,timeout.loading)




        //customMethod.fieldValue(loginLocator.emailField, email);
        //customMethod.fieldValue(loginLocator.passwordField, password);
        //customMethod.clickElement(loginLocator.loginButton);
        //I.waitForElement(settingPageLocator.logoDiv, timeout.waitForElement)
    },
}