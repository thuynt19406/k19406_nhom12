module.exports = {
    urlLoginPage: "https://crosstech-sandbox.casso.vn/login",
    emailField: '#mat-input-0', //"//input[@formcontrolname = 'email']",
    passwordField: "//input[@formcontrolname = 'password']",
    loginButton: "//button[@type='submit']"
}